﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Content;
using ArcimboldoApp.Models;
using ArcimboldoApp.Droid.Models;
using ArcimboldoApp.Models.CodeClasses;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace ArcimboldoApp.Droid.Models
{
    public class CustomPickerRenderer : PickerRenderer
    {
        public CustomPickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            Control.Gravity = GravityFlags.CenterHorizontal;

            var view = (CustomPicker)Element;
            var gradientBackground = new GradientDrawable();

            if (view.IsCurvedCornersEnabled)
            {
                gradientBackground.SetStroke(view.BorderWidth, view.BorderColor.ToAndroid());
                gradientBackground.SetCornerRadius(float.Parse(view.CornerRadius.ToString()));

                Control.SetBackground(gradientBackground);
            }
        }
    }
}