﻿using Android.Content;
using Android.Graphics.Drawables;
using ArcimboldoApp.Droid.Models;
using ArcimboldoApp.Models.CodeClasses;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace ArcimboldoApp.Droid.Models
{
    public class CustomEntryRenderer : EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            CustomEntry view = (CustomEntry)Element;
            GradientDrawable gradientBackground = new GradientDrawable();

            if (view.IsCurvedCornersEnabled)
            {
                gradientBackground.SetStroke(view.BorderWidth, view.BorderColor.ToAndroid());
                gradientBackground.SetCornerRadius(float.Parse(view.CornerRadius.ToString()));

                Control.SetBackground(gradientBackground);
            }
        }
    }
}