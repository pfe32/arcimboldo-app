﻿using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Views;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using Xamarin.Forms;

namespace ArcimboldoApp
{ 
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            if (!String.IsNullOrEmpty(Settings.User)) { tryApiCall(); }
            MainPage = new TabbedBar();
        }

        private async void tryApiCall()
        {
            UserInSettings user = JsonConvert.DeserializeObject<UserInSettings>(Settings.User);
            HttpClient client = new HttpClient();

            try { string content = await client.GetStringAsync("http://pfe32.ddns.net/checkSession?session=" + user.SessionID); }
            catch { Settings.RemoveUserSetting(); }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
