﻿using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.DatabaseClasses;
using ArcimboldoApp.Models.JSONClasses.GET;
using ArcimboldoApp.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Recipe> suggestedRecipes = new ObservableCollection<Recipe>();
        ObservableCollection<Recipe> userRecipes = new ObservableCollection<Recipe>();
        public MainPage()
        {
            InitializeComponent();

            NavigationPage.SetHasBackButton(this, false);

            setSuggestedRecipes();

            if (!String.IsNullOrEmpty(Settings.User)) 
            {
                setUserRecipes(); 
            }

            MessagingCenter.Subscribe<CityInput>(this, "RefreshData", (sender) =>
            {
                suggestedRecipes = new ObservableCollection<Recipe>();
                setSuggestedRecipes();

                if (!String.IsNullOrEmpty(Settings.User))
                {
                    userRecipes = new ObservableCollection<Recipe>();
                    setUserRecipes();
                }
            });

            MessagingCenter.Subscribe<Login>(this, "RefreshData", (sender) =>
            {
                userRecipes = new ObservableCollection<Recipe>();
                setUserRecipes();
            });

            MessagingCenter.Subscribe<CreateAccount>(this, "RefreshData", (sender) =>
            {
                userRecipes = new ObservableCollection<Recipe>();
                setUserRecipes();
            });

            MessagingCenter.Subscribe<OrderFinalization>(this, "RefreshData", (sender) =>
            {
                userRecipes = new ObservableCollection<Recipe>();
                setUserRecipes();
            });

            MessagingCenter.Subscribe<Logout>(this, "RefreshData", (sender) =>
            {
                userRecipes = new ObservableCollection<Recipe>();
                userRecipeTitle.IsVisible = false;
                allUserRecipes.IsVisible = false;
                UserRecipeList.ItemsSource = userRecipes;
            });

            TapGestureRecognizer userTapGestureRecognizer = new TapGestureRecognizer();
            userTapGestureRecognizer.Tapped += OnAllUserRecipesClick;

            allUserRecipes.GestureRecognizers.Add(userTapGestureRecognizer);

            TapGestureRecognizer suggestedTapGestureRecognizer = new TapGestureRecognizer();
            suggestedTapGestureRecognizer.Tapped += OnAllSuggestedRecipesClick;

            allSuggestedRecipes.GestureRecognizers.Add(suggestedTapGestureRecognizer);
        }

        private async void setSuggestedRecipes()
        {
            UserCityInformation city = JsonConvert.DeserializeObject<UserCityInformation>(Settings.UserLocation);
            await InitializeRecipes(suggestedRecipes, "http://pfe32.ddns.net/recipes?latitude=" + city.CityCoordinates.Latitude.ToString("G", CultureInfo.InvariantCulture) + "&longitude=" + city.CityCoordinates.Longitude.ToString("G", CultureInfo.InvariantCulture));
            SuggestedRecipeList.ItemsSource = suggestedRecipes;
        }

        private async void setUserRecipes()
        {
            UserCityInformation city = JsonConvert.DeserializeObject<UserCityInformation>(Settings.UserLocation);
            UserInSettings user = JsonConvert.DeserializeObject<UserInSettings>(Settings.User);
            await InitializeRecipes(userRecipes, "http://pfe32.ddns.net/lastOrderedRecipes?latitude=" + city.CityCoordinates.Latitude.ToString("G", CultureInfo.InvariantCulture) + "&longitude=" + city.CityCoordinates.Longitude.ToString("G", CultureInfo.InvariantCulture) + "&session=" + user.SessionID);
            UserRecipeList.ItemsSource = userRecipes;

            if (!userRecipes.Any())
            {
                userRecipeTitle.IsVisible = false;
                allUserRecipes.IsVisible = false;
            }
            else
            {
                userRecipeTitle.IsVisible = true;
                allUserRecipes.IsVisible = true;
            }
        }

        private async Task InitializeRecipes(ObservableCollection<Recipe> recipes, string apiCall)
        {
            HttpClient client = new HttpClient();
            string content = await client.GetStringAsync(apiCall);
            List<RecipeAsJson> JSONRecipes = JsonConvert.DeserializeObject<ICollection<RecipeAsJson>>(content).ToList();

            foreach (RecipeAsJson JSONRecipe in JSONRecipes)
            {
                JObject Recipe = JSONRecipe.Recipe;
                double recipePrice = 0.0;

                Recipe RecipeToAdd = new Recipe
                {
                    IdRecipe = Int32.Parse(Recipe["idRecipe"].ToString()),
                    NameRecipe = Recipe["nameRecipe"].ToString(),
                    DescriptionRecipe = Recipe["descriptionRecipe"].ToString(),
                    ImageURL = Recipe["imageUrlRecipe"].ToString()
                };

                List<ProductType> correspondingProductTypes = new List<ProductType>();

                foreach (JToken type in Recipe["types"])
                {
                    JToken insideType = type["type"];

                    ProductType productType = new ProductType
                    {
                        IdType = Int32.Parse(insideType["idType"].ToString()),
                        NameType = insideType["nameType"].ToString(),
                        Quantity = Double.Parse(type["quantity"].ToString())
                    };

                    List<Product> correspondingProducts = new List<Product>();

                    foreach (JToken product in insideType["products"])
                    {
                        Product newProduct = new Product
                        {
                            ProductID = Int32.Parse(product["idProduct"].ToString()),
                            Name = product["nameProduct"].ToString(),
                            Description = product["descriptionProduct"].ToString(),
                            QuantityInStock = Double.Parse(product["availableQuantity"].ToString()),
                            UnitType = product["unitProduct"].ToString(),
                            Price = Double.Parse(product["priceProduct"].ToString()),
                            ImageLink = product["imageUrlProduct"].ToString()
                        };

                        JToken productProducer = product["producer"];

                        Producer producer = new Producer
                        {
                            IdProducer = Int32.Parse(productProducer["idProducer"].ToString()),
                            Name = productProducer["nameProducer"].ToString(),
                            Mail = productProducer["emailContact"].ToString(),
                            PhoneNumber = productProducer["phoneNumber"].ToString()
                        };

                        List<MarketDay> marketDays = new List<MarketDay>();

                        foreach (JToken marketDay in productProducer["marketDays"])
                        {
                            JToken JSONMarket = marketDay["market"];

                            MarketDay day = new MarketDay { Day = marketDay["dayMarketDay"].ToString() };

                            JToken JSONAddress = JSONMarket["address"];

                            Market market = new Market
                            {
                                IdMarket = Int32.Parse(JSONMarket["idMarket"].ToString()),
                                Address = new Address
                                {
                                    IdAddress = Int32.Parse(JSONAddress["idAddress"].ToString()),
                                    Street = JSONAddress["streetAddress"].ToString(),
                                    City = JSONAddress["cityAddress"].ToString(),
                                    Country = JSONAddress["countryAddress"].ToString(),
                                    PostalCode = Int32.Parse(JSONAddress["zipCodeAddress"].ToString()),
                                    Latitude = Double.Parse(JSONAddress["latitudeAddress"].ToString()),
                                    Longitude = Double.Parse(JSONAddress["longitudeAddress"].ToString())
                                }
                            };

                            day.Market = market;
                            marketDays.Add(day);
                        }

                        producer.MarketDays = marketDays;
                        newProduct.Producer = producer;
                        correspondingProducts.Add(newProduct);
                    }

                    List<Product> sortedProductList = correspondingProducts.OrderBy(x => x.Price).ToList();

                    productType.Product = sortedProductList[0];
                    correspondingProductTypes.Add(productType);
                    
                }
               
                RecipeToAdd.Types = correspondingProductTypes;

                foreach (ProductType productType in RecipeToAdd.Types) { recipePrice += productType.Quantity * productType.Product.Price; }
                RecipeToAdd.Price = recipePrice;

                recipes.Add(RecipeToAdd);              
            }        
        }

        private void OnRecipeListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CollectionView correspondingView = (CollectionView)sender;

            Recipe selected = e.CurrentSelection.FirstOrDefault() as Recipe;

            if (selected != null)
            {
                Navigation.PushAsync(new RecipeVisualisation(selected));
                correspondingView.SelectedItem = null;
            }
        }

        private void OnAllSuggestedRecipesClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RecipeListPage(suggestedRecipes, "Toutes les recettes du moment", "Recettes du moment"));
        }

        private void OnAllUserRecipesClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RecipeListPage(userRecipes, "Toutes vos dernières recettes", "Dernières recettes"));
        }

        private void OnNewButtonClicked(object sender, EventArgs args)
        {
            NavigationPage navigationParent = this.Parent as NavigationPage;
            TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

            tabbedParent.RefreshBasketNavigationStack();
            tabbedParent.SwitchToBasket();
        }

        private void OnListButtonClicked(object sender, EventArgs args)
        {
            Navigation.PushAsync(new RecipeListPage(suggestedRecipes, "Toutes les recettes", "Toutes les recettes"));
        }
    }
}