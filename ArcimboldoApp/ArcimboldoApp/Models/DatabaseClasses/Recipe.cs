﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    /// <summary>
    /// Panier garni (custom or pre-defined).
    /// </summary>
    public class Recipe
    {
        public int IdRecipe { get; set; }
        public string NameRecipe { get; set; }
        public int DurationRecipe { get; set; }
        public string DescriptionRecipe { get; set; }
        public double Price { get; set; }
        public string ImageURL { get; set; }
        public List<ProductType> Types { get; set; }
    }
}
