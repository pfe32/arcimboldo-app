﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    public class Address
    {
        public int IdAddress { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int PostalCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
