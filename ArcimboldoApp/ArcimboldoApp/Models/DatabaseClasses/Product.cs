﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    /// <summary>
    /// Specific product sold by a producer.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Product name.
        /// </summary>
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double QuantityInStock { get; set; }
        public double QuantityAsked { get; set; }
        public string UnitType { get; set; }
        public double Price { get; set; }
        public string ImageLink { get; set; }
        public Producer Producer { get; set; }
    }
}
