﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    /// <summary>
    /// Product used in a recipe.
    /// </summary>
    public class Ingredient
    {
        /// <summary>
        /// Product
        /// </summary>
        public ProductType ProductType { get; set; }

        /// <summary>
        /// Necesarry quantity of product in the recipe.
        /// </summary>
        public int Quantity { get; set; }
    }
}
