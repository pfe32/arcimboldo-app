﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    /// <summary>
    /// Someone that sells products.
    /// </summary>
    public class Producer
    {
        /// <summary>
        /// Name of the producer/company.
        /// </summary>
        public int IdProducer { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public string PhoneNumber { get; set; }
        public List<Address> Addresses { get; set; }
        public List<MarketDay> MarketDays { get; set; }
    }
}
