﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    public class Market
    {
        public int IdMarket { get; set; }
        public List<Order> Orders { get; set; }
        public Address Address { get; set; }
    }
}
