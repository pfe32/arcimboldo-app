﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    /// <summary>
    /// Large product category (fruit, meat...).
    /// </summary>
    public class ProductCategory
    {
        /// <summary>
        /// Category name.
        /// </summary>
        public string Name { get; set; }
    }
}
