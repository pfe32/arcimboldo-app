﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    /// <summary>
    /// Precise type of product not related to a producer (like "Fraise Mara des Bois").
    /// </summary>
    public class ProductType
    {
        public int IdType { get; set; }
        public string NameType { get; set; }
        public Product Product { get; set; }
        public double Quantity { get; set; }
    }
}
