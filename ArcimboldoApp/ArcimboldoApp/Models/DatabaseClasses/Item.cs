﻿using System;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    public class Item
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}