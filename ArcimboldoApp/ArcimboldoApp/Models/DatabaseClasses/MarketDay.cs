﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.DatabaseClasses
{
    public class MarketDay
    {
        public string Day { get; set; }
        public Market Market { get; set; }
    }
}
