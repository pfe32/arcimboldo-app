﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.JSONClasses.POST
{
    public class UserAsJson
    {
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
