﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.JSONClasses.POST
{
    public class ProductIdAndQuantityAsJson
    {
        public int id { get; set; }
        public double quantity { get; set; }
    }
}
