﻿using ArcimboldoApp.Models.DatabaseClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.JSONClasses.POST
{
    public class OrderAsJson
    {
		public int marketId { get; set; }
		public double totalPrice { get; set; }
		public DateTime marketDate { get; set; }
		public List<ProductIdAndQuantityAsJson> products { get; set; }
	}
}
