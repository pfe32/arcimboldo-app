﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ArcimboldoApp.Models.JSONClasses.GET
{
    public class LocationClass
    {
        [JsonProperty("codesPostaux")]
        public string[] PostalCodes { get; set; }
        [JsonProperty("nom")]
        public string CityName { get; set; }
        [JsonProperty("centre")]
        public JObject centre { get; set; }      
    }
}