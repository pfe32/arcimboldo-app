﻿using ArcimboldoApp.Models.DatabaseClasses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.JSONClasses.GET
{
    public class ProductAsJson
    {
        [JsonProperty("idProduct")]
        public int ProductID { get; set; }
        [JsonProperty("nameProduct")]
        public string Name { get; set; }
        [JsonProperty("descriptionProduct")]
        public string Description { get; set; }
        [JsonProperty("availableQuantity")]
        public double QuantityInStock { get; set; }
        [JsonProperty("unitProduct")]
        public string UnitType { get; set; }
        [JsonProperty("priceProduct")]
        public double Price { get; set; }
        [JsonProperty("imageUrlProduct")]
        public string ImageLink { get; set; }
        [JsonProperty("producer")]
        public JObject Producer { get; set; }
    }
}
