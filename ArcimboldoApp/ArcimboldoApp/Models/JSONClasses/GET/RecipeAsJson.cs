﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.JSONClasses.GET
{
    public class RecipeAsJson
    {
        [JsonProperty("recipe")]
        public JObject Recipe { get; set; }
        [JsonProperty("availability")]
        public JObject Availability { get; set; }
    }
}
