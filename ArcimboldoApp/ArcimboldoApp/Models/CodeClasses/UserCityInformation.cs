﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.CodeClasses
{
    public class UserCityInformation
    {
        public CityAndPostalCode CityNameAndPostalCode {get;set;}
        public Coordinates CityCoordinates { get; set; }
    }
}
