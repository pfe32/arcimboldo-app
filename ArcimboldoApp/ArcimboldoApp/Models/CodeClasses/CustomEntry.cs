﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ArcimboldoApp.Models.CodeClasses
{
    public class CustomEntry : Entry
    {
        private static readonly BindableProperty BorderColorProperty = BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(CustomEntry), Color.Gray);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        private static readonly BindableProperty BorderWidthProperty = BindableProperty.Create(nameof(BorderWidth), typeof(int), typeof(CustomEntry), GetBorderWidth());

        public int BorderWidth
        {
            get { return (int)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        private static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(nameof(CornerRadius), typeof(double), typeof(CustomEntry), GetCornerRadius());

        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        private static readonly BindableProperty IsCurvedCornersEnabledProperty = BindableProperty.Create(nameof(IsCurvedCornersEnabled), typeof(bool),
            typeof(CustomEntry), true);

        public bool IsCurvedCornersEnabled
        {
            get { return (bool)GetValue(IsCurvedCornersEnabledProperty); }
            set { SetValue(IsCurvedCornersEnabledProperty, value); }
        }

        private static int GetBorderWidth()
        {
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    return 2;
                case Device.iOS:
                    return 1;
            }

            return 0;
        }

        private static double GetCornerRadius()
        {
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    return 7;
                case Device.iOS:
                    return 6;
            }

            return 0;
        }
    }
}
