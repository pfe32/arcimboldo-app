﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.CodeClasses
{
    public class DayAndDate
    {
        public string Day { get; set; }
        public DateTime Date { get; set; }
    }
}
