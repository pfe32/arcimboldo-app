﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.CodeClasses
{
    public class Coordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
