﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.CodeClasses
{
    public class UserInSettings
    {
        public string Username { get; set; }
        public string SessionID { get; set; }
    }
}
