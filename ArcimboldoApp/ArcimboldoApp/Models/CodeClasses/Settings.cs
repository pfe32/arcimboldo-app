﻿using ArcimboldoApp.Models;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ArcimboldoApp.Models.CodeClasses
{
    public class Settings
    {
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }
        public static string BasketProducts
        {
            get => AppSettings.GetValueOrDefault(nameof(BasketProducts), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(BasketProducts), value);
        }

        public static string UserLocation
        {
            get => AppSettings.GetValueOrDefault(nameof(UserLocation), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserLocation), value);
        }

        public static string User
        {
            get => AppSettings.GetValueOrDefault(nameof(User), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(User), value);
        }

        public static string ChosenRecipes
        {
            get => AppSettings.GetValueOrDefault(nameof(ChosenRecipes), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(ChosenRecipes), value);
        }

        public static void RemoveSettings()
        {
            AppSettings.Remove(nameof(BasketProducts));
            AppSettings.Remove(nameof(UserLocation));
            AppSettings.Remove(nameof(User));
            AppSettings.Remove(nameof(ChosenRecipes));
        }

        public static void RemoveUserSetting()
        {
            AppSettings.Remove(nameof(User));
        }

        public static void RemoveBasketProductsSetting()
        {
            AppSettings.Remove(nameof(BasketProducts));
        }

        public static void RemoveUserLocationSetting()
        {
            AppSettings.Remove(nameof(UserLocation));
        }

        public static void RemoveChosenRecipesSetting()
        {
            AppSettings.Remove(nameof(ChosenRecipes));
        }
    }
}