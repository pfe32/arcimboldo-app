﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcimboldoApp.Models.CodeClasses
{
    public class CityAndPostalCode
    {
        public string CityName { get; set; }
        public string PostalCode { get; set; }
    }
}
