﻿using ArcimboldoApp.Models;
using ArcimboldoApp.Models.DatabaseClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeListPage : ContentPage
    {
        public RecipeListPage(ObservableCollection<Recipe> recipes, string labelTitle, string pageTitle)
        {
            InitializeComponent();

            RecipeList.ItemsSource = recipes;
            this.Title = pageTitle;
        }

        void OnRecipeListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Recipe selected = e.CurrentSelection.FirstOrDefault() as Recipe;
            if (selected != null)
            {
                Navigation.PushAsync(new RecipeVisualisation(selected));
                RecipeList.SelectedItem = null;
            }
        }
    }
}