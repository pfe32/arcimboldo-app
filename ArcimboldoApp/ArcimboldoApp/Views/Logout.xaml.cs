﻿using ArcimboldoApp.Models.CodeClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Logout : ContentPage
    {
        public Logout()
        {
            InitializeComponent();

            NavigationPage.SetHasBackButton(this, false);

            userName.Text = "Connecté en tant que \n" + JsonConvert.DeserializeObject<UserInSettings>(Settings.User).Username;
            userIdentity.Text = JsonConvert.DeserializeObject<UserInSettings>(Settings.User).Username;
        }

        private async void OnLogoutClick(object sender, EventArgs e)
        {
            Settings.RemoveUserSetting();

            NavigationPage navigationParent = this.Parent as NavigationPage;
            TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

            tabbedParent.RefreshMainPageNavigationStack();

            await Task.Delay(500);

            MessagingCenter.Send(this, "RefreshData");

            tabbedParent.RefreshBasketNavigationStack();
            tabbedParent.ChangeLogTabAttributesToLogin();
            tabbedParent.SwitchToMainPage();
        }
    }
}