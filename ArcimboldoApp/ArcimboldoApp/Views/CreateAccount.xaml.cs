﻿using ArcimboldoApp.Models;
using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.JSONClasses.POST;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateAccount : ContentPage
    {
        public CreateAccount()
        {
            InitializeComponent();

            NavigationPage.SetHasBackButton(this, false);

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += OnAlreadyHaveAccountClick;

            alreadyAnAccount.GestureRecognizers.Add(tapGestureRecognizer);
        }

        private void OnAlreadyHaveAccountClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Login());
            Navigation.RemovePage(this);
        }

        private async void OnCreateAccountClick(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            Uri uri = new Uri("http://pfe32.ddns.net/newAccount");
            string username = firstName.Text + name.Text;

            UserAsJson user = new UserAsJson
            {
                username = username,
                email = email.Text,
                password = password.Text
            };

            string userToJson = JsonConvert.SerializeObject(user);
            StringContent content = new StringContent(userToJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, content);
            JToken result = JsonConvert.DeserializeObject<JToken>(await response.Content.ReadAsStringAsync());

            if (result["error"] != null)
            {
                await DisplayAlert("Problème de création de compte", "Ce mail est déjà utilisé.", "OK");
            }
            else
            {
                UserInSettings userInSettings = new UserInSettings
                {
                    Username = username,
                    SessionID = result["session"].ToString()
                };

                Settings.User = JsonConvert.SerializeObject(userInSettings);

                NavigationPage navigationParent = this.Parent as NavigationPage;
                TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

                MessagingCenter.Send(this, "RefreshData");
                tabbedParent.ChangeLogTabAttributesToLogout();
                tabbedParent.RefreshMainPageNavigationStack();
                tabbedParent.SwitchToMainPage();
            }
        }
    }
}