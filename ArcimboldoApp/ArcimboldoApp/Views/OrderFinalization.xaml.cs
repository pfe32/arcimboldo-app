﻿using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.DatabaseClasses;
using ArcimboldoApp.Models.JSONClasses.POST;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderFinalization : ContentPage
    {
        DateTime chosenDate = new DateTime(); 
        List<DayAndDate> marketDays = new List<DayAndDate>();
        public OrderFinalization()
        {
            InitializeComponent();
            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);
            Address chosenAddress = products[0].Producer.MarketDays[0].Market.Address;

            if (chosenAddress.City == "Versailles")
            {
                topImage.Source = "versaillesmarket.jpg";
                cityAddress.Text = "Place du marché Notre-Dame, 78000 Versailles";
            }
            else
            {
                topImage.Source = "marseillemarket.jpg";
                cityAddress.Text = "Place Jean-Jaurès, 13005 Marseille";
            }

            ScrollView productListScroll = new ScrollView();
            StackLayout productLayoutList = new StackLayout();

            foreach (Product p in products)
            {
                foreach (MarketDay marketDay in p.Producer.MarketDays)
                {
                    string translatedMarketDay = "";
                    DateTime correspondingDate = new DateTime();

                    switch (marketDay.Day)
                    {
                        case "MONDAY":
                            translatedMarketDay = "LUNDI";
                            correspondingDate = getCorrespondingDate(DayOfWeek.Monday);
                            break;
                        case "TUESDAY":
                            translatedMarketDay = "MARDI";
                            correspondingDate = getCorrespondingDate(DayOfWeek.Tuesday);
                            break;
                        case "WEDNESDAY":
                            translatedMarketDay = "MERCREDI";
                            correspondingDate = getCorrespondingDate(DayOfWeek.Wednesday);
                            break;
                        case "THURSDAY":
                            translatedMarketDay = "JEUDI";
                            correspondingDate = getCorrespondingDate(DayOfWeek.Thursday);
                            break;
                        case "FRIDAY":
                            translatedMarketDay = "VENDREDI";
                            correspondingDate = getCorrespondingDate(DayOfWeek.Friday);
                            break;
                        case "SATURDAY":
                            translatedMarketDay = "SAMEDI";
                            correspondingDate = getCorrespondingDate(DayOfWeek.Saturday);
                            break;
                        case "SUNDAY":
                            translatedMarketDay = "DIMANCHE";
                            correspondingDate = getCorrespondingDate(DayOfWeek.Sunday);
                            break;
                    }

                    DayAndDate dayAndDate = new DayAndDate
                    {
                        Day = translatedMarketDay,
                        Date = correspondingDate
                    };

                    if (!marketDays.Any(d => d.Day == dayAndDate.Day))
                    {
                        marketDays.Add(dayAndDate);
                    }
                }

                StackLayout productLayout = new StackLayout { Orientation = StackOrientation.Horizontal };

                Image productImage = new Image { Source = p.ImageLink, WidthRequest = 45, HeightRequest = 45 };

                FormattedString productDescriptionText = new FormattedString();
                productDescriptionText.Spans.Add(new Span { Text = p.Name, ForegroundColor = Color.FromHex("#087f23"), FontAttributes = FontAttributes.Bold });
                productDescriptionText.Spans.Add(new Span { Text = "\n" });
                productDescriptionText.Spans.Add(new Span { Text = "Prix : " });
                productDescriptionText.Spans.Add(new Span { Text = p.Price + "€/" + p.UnitType.ToLower() });

                Label productDescription = new Label
                {
                    FormattedText = productDescriptionText,
                    VerticalTextAlignment = TextAlignment.Center,
                    Margin = new Thickness(0, 0, 10, 0)
                };

                Label quantityLabel = new Label
                {
                    Text = "Quantité : " + (p.UnitType == "PIECE" ? p.QuantityAsked.ToString() : p.QuantityAsked * 1000 + "g"),
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                };

                productLayout.Children.Add(productImage);
                productLayout.Children.Add(productDescription);
                productLayout.Children.Add(quantityLabel);

                productLayoutList.Children.Add(productLayout);
            }

            productListScroll.Content = productLayoutList;
            summaryOrder.Content = productListScroll;

            int count = 0;
            StackLayout dayLayout = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center };
            List<DayAndDate> sortedMarketDays = marketDays.OrderBy(o => o.Date).ToList();

            foreach (DayAndDate day in sortedMarketDays)
            {
                Frame dayFrame = new Frame
                {
                    AutomationId = day.Day,
                    BorderColor = Color.Black,
                    CornerRadius = 10,
                    BackgroundColor = Color.Transparent
                };

                TapGestureRecognizer frameTapGestureRecognizer = new TapGestureRecognizer();
                frameTapGestureRecognizer.Tapped += OnDayClick;

                dayFrame.GestureRecognizers.Add(frameTapGestureRecognizer);

                Label dayLabel = new Label
                {
                    Text = day.Day,
                    HorizontalTextAlignment = TextAlignment.Center
                };

                dayFrame.Content = dayLabel;
                dayLayout.Children.Add(dayFrame);

                count++;

                if(count % 3 == 2)
                { 
                    marketDaysLayout.Children.Add(dayLayout);
                    dayLayout = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center };
                }
            }
        }

        private void OnDayClick(object sender, EventArgs e)
        {
            Frame correspondingFrame = (Frame)sender;
            Label correspondingLabel = new Label();
            String correspondingId = correspondingFrame.AutomationId;

            if (correspondingFrame.BackgroundColor == Color.Transparent)
            {
                foreach (StackLayout i in marketDaysLayout.Children.Where(x => x.GetType() == typeof(StackLayout)))
                {
                    foreach (Frame j in i.Children.Where(x => x.GetType() == typeof(Frame)))
                    {
                        if (j.AutomationId != correspondingId)
                        {
                            j.BackgroundColor = Color.Transparent;
                        }
                    }
                }

                correspondingFrame.BackgroundColor = Color.FromHex("#80e27e");

                foreach (Label i in correspondingFrame.Children.Where(x => x.GetType() == typeof(Label)))
                {
                    correspondingLabel = i;
                }

                chosenDate = marketDays.Find(x => x.Day == correspondingLabel.Text).Date;
                chosenDay.Text = "Jour du marché : " + chosenDate.ToLongDateString();
                chosenDay.IsVisible = true;
            }
            else
            {
                correspondingFrame.BackgroundColor = Color.Transparent;
                chosenDay.IsVisible = false;
            }
        } 

        private async void OnOrderClick(object sender, EventArgs e)
        {
            if(chosenDate == new DateTime())
            {
                await DisplayAlert("Problème de date", "Vous n'avez pas choisi de jour pour la livraison au marché.", "OK");
            }
            else
            {
                registerOrderedRecipes();
                sendOrder();
                
                Settings.RemoveBasketProductsSetting();
                Settings.RemoveChosenRecipesSetting();

                NavigationPage navigationParent = this.Parent as NavigationPage;
                TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

                await DisplayAlert("Commande confirmée", "Votre commande nous a bien été envoyée, elle sera livrée le jour choisi au marché correspondant.", "OK");

                tabbedParent.RefreshMainPageNavigationStack();

                await Task.Delay(500);

                MessagingCenter.Send(this, "RefreshData");

                tabbedParent.SwitchToMainPage();
                tabbedParent.RefreshBasketNavigationStack();
            }
        }

        private DateTime getCorrespondingDate(DayOfWeek dayOfWeek)
        {
            return DateTime.Today.AddDays(((int)dayOfWeek - (int)DateTime.Today.DayOfWeek + 7) % 7);
        }

        private async void registerOrderedRecipes()
        {
            UserInSettings userInSettings = JsonConvert.DeserializeObject<UserInSettings>(Settings.User);

            HttpClient client = new HttpClient();
            Uri uri = new Uri("http://pfe32.ddns.net/addOrderedRecipes?session=" + userInSettings.SessionID);

            List<int> recipeIds = JsonConvert.DeserializeObject<List<int>>(Settings.ChosenRecipes);

            string recipeIdsToJson = JsonConvert.SerializeObject(recipeIds);

            StringContent content = new StringContent(recipeIdsToJson, Encoding.UTF8, "application/json");
            await client.PostAsync(uri, content);
        }

        private async void sendOrder()
        {
            UserInSettings userInSettings = JsonConvert.DeserializeObject<UserInSettings>(Settings.User);
            List<Product> basketProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);

            List<ProductIdAndQuantityAsJson> products = new List<ProductIdAndQuantityAsJson>();
            int marketId = basketProducts[0].Producer.MarketDays[0].Market.IdMarket;
            double basketPrice = 0.0;

            HttpClient client = new HttpClient();
            Uri uri = new Uri("http://pfe32.ddns.net/order?session=" + userInSettings.SessionID);

            foreach(Product p in basketProducts)
            {
                products.Add(new ProductIdAndQuantityAsJson
                {
                    id = p.ProductID,
                    quantity = p.QuantityAsked
                });

                basketPrice += p.Price * p.QuantityAsked;
            }

            OrderAsJson orderAsJson = new OrderAsJson
            {
                marketId = marketId,
                totalPrice = Math.Round(basketPrice, 2),
                marketDate = chosenDate,
                products = products
            };

            string orderToJson = JsonConvert.SerializeObject(orderAsJson);

            StringContent content = new StringContent(orderToJson, Encoding.UTF8, "application/json");
            await client.PostAsync(uri, content);
        }
    }
}