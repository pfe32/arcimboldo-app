﻿using ArcimboldoApp.Models;
using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.DatabaseClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PersonalizedBasketTool : ContentPage
    {      
        Frame productListFrame;
        Label basketPrice;
        public PersonalizedBasketTool()
        {
            InitializeComponent();

            NavigationPage.SetHasBackButton(this, false);

            StackLayout finalLayout = new StackLayout { Margin = new Thickness(0, 0, 0, 10) };
            StackLayout buttonLayout = new StackLayout { Orientation = StackOrientation.Horizontal };

            ImageButton addProduct = new ImageButton
            {
                Source = "addToBasket.png",
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                BackgroundColor = Color.Transparent,
                HeightRequest = 50,
                WidthRequest = 50,
                Margin = new Thickness(20, 10, 0, 0) 
            };

            addProduct.Clicked += OnAddProductClick;

            ImageButton emptyBasket = new ImageButton
            {
                Source = "delete.png",
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.Start,
                BackgroundColor = Color.Transparent,
                HeightRequest = 50,
                WidthRequest = 50,
                Margin = new Thickness(0, 10, 20, 0)
            };

            emptyBasket.Clicked += OnEmptyListClick;

            basketPrice = new Label { FontSize = 60, TextColor = Color.Black, HorizontalTextAlignment = TextAlignment.Center };

            Button orderBasketButton = new Button
            {
                BackgroundColor = Color.FromHex("#80e27e"),
                FontFamily = "sans-serif",
                TextColor = Color.White,
                CornerRadius = 15,
                FontSize = 25,
                Padding = new Thickness(25,15,25,15),
                Margin = new Thickness(10, 0, 10, 0),
                VerticalOptions = LayoutOptions.End
            };

            productListFrame = new Frame { BorderColor = Color.Black, Margin = new Thickness(5, 0, 5, 2) };
          
            UpdateComponentsWithUserBasket();

            MessagingCenter.Subscribe<CityInput>(this, "RefreshData", (sender) =>
            {
                UpdateComponentsWithUserBasket();
            });

            MessagingCenter.Subscribe<RecipeVisualisation>(this, "RefreshData", (sender) =>
            {
                UpdateComponentsWithUserBasket();
            });

            MessagingCenter.Subscribe<AddProductInBasket>(this, "RefreshData", (sender) =>
            {
                UpdateComponentsWithUserBasket();
            });

            MessagingCenter.Subscribe<OrderFinalization>(this, "RefreshData", (sender) =>
            {
                UpdateComponentsWithUserBasket();
            });

            buttonLayout.Children.Add(addProduct);
            buttonLayout.Children.Add(emptyBasket);

            finalLayout.Children.Add(buttonLayout);
            finalLayout.Children.Add(basketPrice);
            finalLayout.Children.Add(productListFrame);
            finalLayout.Children.Add(orderBasketButton);

            orderBasketButton.Text = "COMMANDER";
            orderBasketButton.Clicked += OnOrderBasketButtonClick;

            this.Content = finalLayout;
        }   

        private double returnBasketPrice()
        {
            double basketPriceValue = 0.0;
            List<Product> basketProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);

            foreach(Product p in basketProducts)
            {
                basketPriceValue += p.QuantityAsked * p.Price;
            }

            return Math.Round(basketPriceValue, 2);
        }

        private Tuple<ScrollView, double> createListScrollView(List<Product> products, bool isRecipe)
        {
            ScrollView productListScroll = new ScrollView();
            StackLayout productLayoutList = new StackLayout();

            foreach (Product p in products)
            {
                StackLayout finalLayout = new StackLayout { AutomationId = "productLayout:" + p.ProductID, Margin = new Thickness(0, 0, 0, 20) };
                StackLayout productLayout = new StackLayout { Orientation = StackOrientation.Horizontal };

                ImageButton removeProduct = new ImageButton
                {
                    AutomationId = "Remove:" + p.ProductID,
                    Source = "remove.png",
                    WidthRequest = 25,
                    HeightRequest = 25,
                    VerticalOptions = LayoutOptions.Center
                };

                removeProduct.Clicked += OnRemoveProductClick;

                Image productImage = new Image { Source = p.ImageLink, WidthRequest = 55, HeightRequest = 55 };

                FormattedString productDescriptionText = new FormattedString();
                productDescriptionText.Spans.Add(new Span { Text = p.Name, ForegroundColor = Color.FromHex("#087f23"), FontAttributes = FontAttributes.Bold });
                productDescriptionText.Spans.Add(new Span { Text = "\n" });
                productDescriptionText.Spans.Add(new Span { Text = "Prix : " });
                productDescriptionText.Spans.Add(new Span { Text = p.Price + "€/" + p.UnitType.ToLower() });

                Label productDescription = new Label
                {
                    FormattedText = productDescriptionText,
                    VerticalTextAlignment = TextAlignment.Center,
                    Margin = new Thickness(0,0,10,0)
                };

                CustomEntry quantityPicker = new CustomEntry
                {
                    AutomationId = "quantityPicker:" + p.ProductID,
                    Text = p.UnitType == "PIECE" ? p.QuantityAsked.ToString() : (p.QuantityAsked * 1000).ToString(),
                    WidthRequest = p.UnitType == "PIECE" ? 60 : 65,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Keyboard = Keyboard.Numeric
                };

                quantityPicker.TextChanged += OnQuantityChange;

                Label unitLabel = new Label
                {
                    Text = "g",
                    FontSize = 20,
                    VerticalTextAlignment = TextAlignment.Center
                };

                Label quantityLabel = new Label
                {
                    AutomationId = "quantityLabel" + p.ProductID,
                    HorizontalTextAlignment = TextAlignment.Center,
                    TextColor = Color.Red,
                    IsVisible = false
                };
    
                productLayout.Children.Add(removeProduct);
                productLayout.Children.Add(productImage);
                productLayout.Children.Add(productDescription);
                productLayout.Children.Add(quantityPicker);

                if(p.UnitType == "KG") { productLayout.Children.Add(unitLabel); }

                finalLayout.Children.Add(productLayout);
                finalLayout.Children.Add(quantityLabel);

                productLayoutList.Children.Add(finalLayout);
            }

            productListScroll.Content = productLayoutList;

            return new Tuple<ScrollView, double>(productListScroll, returnBasketPrice());
        }

        private void UpdateComponentsWithUserBasket()
        {
            List<Product> userProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);
            double basketPriceValue = 0.0;

            if (userProducts != null && userProducts.Count != 0)
            {
                Tuple<ScrollView, double> scrollAndPrice = createListScrollView(userProducts, false);
                productListFrame.Content = scrollAndPrice.Item1;
                basketPriceValue = scrollAndPrice.Item2;
            }
            else
            {
                StackLayout emptyBasketLayout = new StackLayout();
                Image emptyBox = new Image
                {
                    Source = "box.png",
                    HorizontalOptions = LayoutOptions.Center,
                    BackgroundColor = Color.Transparent,
                    WidthRequest = 65,
                    HeightRequest = 65,
                    Margin = new Thickness(0,0,0,5)
                };

                Label informationLabel = new Label
                {
                    Text = "VOTRE PANIER EST VIDE, AJOUTEZ-Y QUELQUE CHOSE !",
                    FontAttributes = FontAttributes.Bold,
                    HorizontalTextAlignment = TextAlignment.Center
                };

                emptyBasketLayout.Children.Add(emptyBox);
                emptyBasketLayout.Children.Add(informationLabel);

                productListFrame.VerticalOptions = LayoutOptions.CenterAndExpand;
                productListFrame.Content = emptyBasketLayout;
            }

            basketPrice.Text = string.Format("{0:N2}€", basketPriceValue);
        }

        private void OnEmptyListClick(object sender, EventArgs e)
        {
            Settings.RemoveBasketProductsSetting();
            Settings.RemoveChosenRecipesSetting();
            UpdateComponentsWithUserBasket();
        }

        private void OnRemoveProductClick(object sender, EventArgs e)
        {
            ImageButton correspondingImageButton = (ImageButton)sender;
            string[] splitId = correspondingImageButton.AutomationId.Split(':');
            List<Product> userProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);
            Product correspondingProduct = userProducts.Find(x => x.ProductID == Int32.Parse(splitId[1]));

            RemoveProductsFromList(correspondingProduct.Name);
            UpdateComponentsWithUserBasket();
        }

        private void RemoveProductsFromList(string productName)
        {
            List<Product> userProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);
            Product correspondingProduct = userProducts.Find(x => x.Name == productName);

            userProducts.Remove(correspondingProduct);
            Settings.BasketProducts = JsonConvert.SerializeObject(userProducts);
        }

        private void OnAddProductClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddProductInBasket());
        }

        private List<int> isAnyQuantityWrong()
        {
            List<int> labelsIds = new List<int>();

            foreach (ScrollView i in productListFrame.Children.Where(x => x.GetType() == typeof(ScrollView)))
            {
                foreach (StackLayout j in i.Children.Where(x => x.GetType() == typeof(StackLayout)))
                {
                    foreach (StackLayout k in j.Children.Where(x => x.GetType() == typeof(StackLayout)))
                    {
                        foreach (Label l in k.Children.Where(x => x.GetType() == typeof(Label)))
                        {
                            if (l.IsVisible == true)
                            {
                                labelsIds.Add(Int32.Parse(l.AutomationId.Split(':')[1]));
                            }
                        }
                    }
                }
            }

            return labelsIds;
        }

        private async void OnOrderBasketButtonClick(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Settings.BasketProducts))
            {
                await DisplayAlert("Problème de panier", "Votre panier est vide.", "OK");
            }
            else
            {
                if (String.IsNullOrEmpty(Settings.User))
                {
                    bool result = await DisplayAlert("Problème d'identification", "Il semblerait que vous n'êtes pas connecté, voulez-vous être redirigé vers la page de connexion ?", "Oui", "Non");

                    if (result == true)
                    {
                        NavigationPage navigationParent = this.Parent as NavigationPage;
                        TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

                        tabbedParent.SwitchToLogin();
                    }
                }
                else
                {
                    List<Product> basketProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);
                    List<Product> alternateList = new List<Product>();

                    List<int> labelsIds = isAnyQuantityWrong();

                    if (labelsIds.Any())
                    {
                        foreach (Product p in basketProducts)
                        {
                            if (labelsIds.Contains(p.ProductID))
                            {
                                await DisplayAlert("Problème de quantité", "Problème rencontré avec la quantité pour le produit " + p.Name + ".", "OK");
                            }
                        }
                    }
                    else
                    {
                        foreach (Product p in basketProducts)
                        {
                            if (p.QuantityAsked != 0)
                            {
                                alternateList.Add(p);
                            }
                        }

                        Settings.BasketProducts = JsonConvert.SerializeObject(alternateList);

                        await Navigation.PushAsync(new OrderFinalization());
                        UpdateComponentsWithUserBasket();
                    }
                }
            }
        }
        
        private void OnQuantityChange(object sender, TextChangedEventArgs e)
        {
            double quantityAsked = 0.0;

            Label correspondingLabel = new Label();
            CustomEntry correspondingEntry = (CustomEntry)sender;

            string[] splitId = correspondingEntry.AutomationId.Split(':');

            List<Product> userProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);
            Product product = userProducts.Where(x => x.ProductID == Int32.Parse(splitId[1])).First();

            foreach (ScrollView i in productListFrame.Children.Where(x => x.GetType() == typeof(ScrollView)))
            {
                foreach (StackLayout j in i.Children.Where(x => x.GetType() == typeof(StackLayout)))
                {
                    foreach (StackLayout k in j.Children.Where(x => x.GetType() == typeof(StackLayout)))
                    {
                        if (splitId[1] == k.AutomationId.Split(':')[1])
                        {
                            foreach (Label l in k.Children.Where(x => x.GetType() == typeof(Label)))
                            {
                                correspondingLabel = l;
                            }
                        }
                    }
                }
            }

            if (String.IsNullOrEmpty(e.NewTextValue))
            {
                correspondingLabel.IsVisible = false;
                correspondingLabel.Text = "";

                product.QuantityAsked = 0;
            }
            else
            {
                if (Regex.IsMatch(e.NewTextValue, product.UnitType == "PIECE" ? "^[0-9]{1,2}?$" : "^[0-9]{1,5}([,][0-9]{1,3})?$"))
                {
                    correspondingLabel.IsVisible = false;
                    correspondingLabel.Text = "";

                    quantityAsked = product.UnitType == "PIECE" ? Double.Parse(e.NewTextValue) : Double.Parse(e.NewTextValue) / 1000;

                    if (quantityAsked > product.QuantityInStock)
                    {
                        correspondingLabel.IsVisible = true;
                        correspondingLabel.Text = "Quantité indisponible.";

                        product.QuantityAsked = 0;
                    }
                    else
                    {
                        correspondingLabel.IsVisible = false;
                        correspondingLabel.Text = "";

                        product.QuantityAsked = quantityAsked;                    
                    }
                }
                else
                {
                    correspondingLabel.IsVisible = true;
                    correspondingLabel.Text = "Quantité invalide.";

                    product.QuantityAsked = 0;
                }
            }

            Settings.BasketProducts = JsonConvert.SerializeObject(userProducts);
            basketPrice.Text = returnBasketPrice() + "€";
        }
    }
}