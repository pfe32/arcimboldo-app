﻿using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.DatabaseClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeVisualisation : ContentPage
    {
        Frame productListFrame;
        Label basketPrice;
        Recipe recipe;
        public RecipeVisualisation(Recipe recipe)
        {
            InitializeComponent();

            this.recipe = recipe;
            this.Title = recipe.NameRecipe;

            StackLayout finalLayout = new StackLayout();

            Label recipeName = new Label
            {
                Text = recipe.NameRecipe,
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = 25
            };

            basketPrice = new Label { FontSize = 35, HorizontalTextAlignment = TextAlignment.Center };

            Button orderBasketButton = new Button
            {
                BackgroundColor = Color.FromHex("#80e27e"),
                Text = "AJOUTER AU PANIER",
                FontFamily = "sans-serif",
                TextColor = Color.White,
                CornerRadius = 15,
                FontSize = 20,
                Padding = new Thickness(25, 15, 25, 15),
                Margin = new Thickness(10, 0, 10, 10),
                VerticalOptions = LayoutOptions.End
            };

            orderBasketButton.Clicked += AddRecipeIngredientsToBasket;

            productListFrame = new Frame { BorderColor = Color.Black, Margin = new Thickness(5, 0, 5, 2) };

            UpdateComponentsWithRecipe();

            Image recipeImage = new Image
            {
                Source = recipe.ImageURL,
                VerticalOptions = LayoutOptions.Start,
                Aspect = Aspect.AspectFill,
                HeightRequest = 250
            };

            finalLayout.Children.Add(recipeImage);
            finalLayout.Children.Add(basketPrice);
            finalLayout.Children.Add(productListFrame);
            finalLayout.Children.Add(orderBasketButton);

            this.Content = finalLayout;
        }

        private void AddRecipeIngredientsToBasket(object sender, EventArgs e)
        {
            List<Product> userProducts = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);

            if (userProducts == null) { userProducts = new List<Product>(); }

            foreach (ProductType productType in recipe.Types)
            {
                Product productToAdd = productType.Product;
                Product existingProduct = userProducts.Find(x => x.ProductID == productToAdd.ProductID);

                if (existingProduct == null)
                {
                    productToAdd.QuantityAsked = productType.Quantity;
                    userProducts.Add(productToAdd);
                }
                else
                {
                    existingProduct.QuantityAsked += productType.Quantity;
                }
            }

            Settings.BasketProducts = JsonConvert.SerializeObject(userProducts);

            if (String.IsNullOrEmpty(Settings.ChosenRecipes))
            {
                List<int> recipeIds = new List<int>();
                recipeIds.Add(recipe.IdRecipe);

                Settings.ChosenRecipes = JsonConvert.SerializeObject(recipeIds);
            }
            else
            {
                List<int> recipeIds = JsonConvert.DeserializeObject<List<int>>(Settings.ChosenRecipes);
                recipeIds.Add(recipe.IdRecipe);

                Settings.ChosenRecipes = JsonConvert.SerializeObject(recipeIds);
            }

            NavigationPage navigationParent = this.Parent as NavigationPage;
            TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

            MessagingCenter.Send(this, "RefreshData");
            tabbedParent.RefreshBasketNavigationStack();
            tabbedParent.SwitchToBasket();
        }

        private Tuple<ScrollView, double> createListScrollView(List<Product> products, bool isRecipe)
        {
            ScrollView productListScroll = new ScrollView();
            StackLayout productLayoutList = new StackLayout();

            double basketPriceValue = 0.0;

            foreach (Product p in products)
            {
                StackLayout productLayout = new StackLayout { Orientation = StackOrientation.Horizontal, Margin = new Thickness(0, 0, 0, 20) };

                Image productImage = new Image { Source = p.ImageLink, WidthRequest = 55, HeightRequest = 55 };


                Label productQuantity = new Label 
                {
                    Text = "Quantité : " + (p.UnitType == "PIECE" ? p.QuantityAsked.ToString() : p.QuantityAsked * 1000 + "g"),
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                };

                FormattedString productDescriptionText = new FormattedString();
                productDescriptionText.Spans.Add(new Span { Text = p.Name, ForegroundColor = Color.FromHex("#087f23"), FontAttributes = FontAttributes.Bold });
                productDescriptionText.Spans.Add(new Span { Text = "\n" });
                productDescriptionText.Spans.Add(new Span { Text = "Prix : " });
                productDescriptionText.Spans.Add(new Span { Text = p.Price + "€/" + p.UnitType.ToLower() });

                Label productDescription = new Label
                {
                    FormattedText = productDescriptionText,
                    VerticalTextAlignment = TextAlignment.Center
                };

                basketPriceValue += p.QuantityAsked * p.Price;

                productLayout.Children.Add(productImage);
                productLayout.Children.Add(productDescription);
                productLayout.Children.Add(productQuantity);

                productLayoutList.Children.Add(productLayout);
            }

            productListScroll.Content = productLayoutList;

            return new Tuple<ScrollView, double>(productListScroll, basketPriceValue);
        }

        private void UpdateComponentsWithRecipe()
        {
            List<Product> recipeProducts = new List<Product>();

            foreach (ProductType productType in recipe.Types)
            {
                productType.Product.QuantityAsked = productType.Quantity;
                recipeProducts.Add(productType.Product);
            }

            Tuple<ScrollView, double> scrollAndPrice = createListScrollView(recipeProducts, true);
            productListFrame.Content = scrollAndPrice.Item1;
            basketPrice.Text = string.Format("{0:N2} €", scrollAndPrice.Item2);
        }
    }
}