﻿using ArcimboldoApp.Models;
using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.JSONClasses.POST;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += OnNoAccountClick;

            noAccount.GestureRecognizers.Add(tapGestureRecognizer);
        }

        private void OnNoAccountClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CreateAccount());
            Navigation.RemovePage(this);
        }

        private async void OnLoginClick(object sender, EventArgs e)
        {           
            HttpClient client = new HttpClient();
            Uri uri = new Uri("http://pfe32.ddns.net/login");

            UserAsJson user = new UserAsJson
            {
                email = email.Text,
                password = password.Text
            };

            string userToJson = JsonConvert.SerializeObject(user);
            StringContent content = new StringContent(userToJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, content);
            JToken result = JsonConvert.DeserializeObject<JToken>(await response.Content.ReadAsStringAsync());
            
            if (result["error"] != null)
            {
                await DisplayAlert("Problème de connexion", "Identifiants incorrects.", "OK");
            }
            else
            {
                JToken userJson = result["user"];
                
                UserInSettings userInSettings = new UserInSettings
                {
                    Username = userJson["surnameUser"].ToString(),
                    SessionID = result["session"].ToString()
                };

                Settings.User = JsonConvert.SerializeObject(userInSettings);

                NavigationPage navigationParent = this.Parent as NavigationPage;
                TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

                MessagingCenter.Send(this, "RefreshData");
                tabbedParent.ChangeLogTabAttributesToLogout();
                tabbedParent.RefreshMainPageNavigationStack();
                tabbedParent.SwitchToMainPage();
            } 
        }
    }
}