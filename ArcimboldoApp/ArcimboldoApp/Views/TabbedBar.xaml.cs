﻿using ArcimboldoApp.Models.CodeClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedBar : TabbedPage
    {
        Page cityPage;
        Page mainPage;
        Page basketPage;
        Page loginPage;
        public TabbedBar()
        {
            InitializeComponent();

            this.BarTextColor = Color.White;
            this.BarBackgroundColor = Color.FromHex("#4caf50");
            this.UnselectedTabColor = Color.White;

            cityPage = new NavigationPage(new CityInput())
            {
                BarBackgroundColor = Color.FromHex("#087f23"),
                BarTextColor = Color.White,
                Title = "Ville"
            };

            if (Device.RuntimePlatform == Device.iOS)
            {
                cityPage.IconImageSource = "loc";
            }

            Children.Add(cityPage);
        }

        public void SwitchToCityInput()
        {           
            CurrentPage = cityPage;
        }

        public void SwitchToMainPage()
        {
            CurrentPage = mainPage;
        }

        public void SwitchToBasket()
        {
            CurrentPage = basketPage;
        }

        public void SwitchToLogin()
        {
            CurrentPage = loginPage;
        }

        public void RefreshMainPageNavigationStack()
        {
            this.Children[1].Navigation.PopToRootAsync();
        }

        public void RefreshBasketNavigationStack()
        {
            this.Children[2].Navigation.PopToRootAsync();
        }

        public void ChangeLogTabAttributesToLogout()
        {
            loginPage = new NavigationPage(new Logout())
            {
                BarBackgroundColor = Color.FromHex("#087f23"),
                BarTextColor = Color.White,
                Title = "Compte"
            };

            if (Device.RuntimePlatform == Device.iOS)
            {
                loginPage.IconImageSource = "logout";
            }
            
            this.Children[3] = loginPage;
        }

        public void ChangeLogTabAttributesToLogin()
        {
            loginPage = new NavigationPage(new Login())
            {
                BarBackgroundColor = Color.FromHex("#087f23"),
                BarTextColor = Color.White,
                Title = "Compte"
            };

            if (Device.RuntimePlatform == Device.iOS)
            {
                loginPage.IconImageSource = "login";
            }

            this.Children[3] = loginPage;
        }

        public int GetNumberOfTabs()
        {
            int nbrPages = Children.Count;
            return nbrPages;
        }

        public void AddTabsAfterCityInput()
        {
            mainPage = new NavigationPage(new MainPage())
            {
                BarBackgroundColor = Color.FromHex("#087f23"),
                BarTextColor = Color.White,
                Title = "Accueil"
            };

            basketPage = new NavigationPage(new PersonalizedBasketTool())
            {
                BarBackgroundColor = Color.FromHex("#087f23"),
                BarTextColor = Color.White,
                Title = "Panier"
            };

            if(String.IsNullOrEmpty(Settings.User))
            {
                loginPage = new NavigationPage(new Login())
                {
                    BarBackgroundColor = Color.FromHex("#087f23"),
                    BarTextColor = Color.White,
                    Title = "Compte"
                };

                if (Device.RuntimePlatform == Device.iOS)
                {
                    loginPage.IconImageSource = "login";
                }
            }
            else
            {
                loginPage = new NavigationPage(new Logout())
                {
                    BarBackgroundColor = Color.FromHex("#087f23"),
                    BarTextColor = Color.White,
                    Title = "Compte"
                };

                if (Device.RuntimePlatform == Device.iOS)
                {
                    loginPage.IconImageSource = "logout";
                }
            }

            if (Device.RuntimePlatform == Device.iOS)
            {
                mainPage.IconImageSource = "home";
                basketPage.IconImageSource = "basket";
            }

            Children.Add(mainPage);
            Children.Add(basketPage);
            Children.Add(loginPage);
        }
    }
}