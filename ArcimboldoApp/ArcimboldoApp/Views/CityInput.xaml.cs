﻿using ArcimboldoApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.JSONClasses.GET;
using ArcimboldoApp.Models.JSONClasses.POST;
using System.Threading.Tasks;
using ArcimboldoApp.Models.DatabaseClasses;
using System.Text;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CityInput : ContentPage
    {
        ListView citiesListView;
        CustomEntry cityEntry;

        List<Coordinates> citiesCoordinates;
        UserCityInformation chosenCityInformation;
        int eventLock = 0;
        public CityInput()
        {
            InitializeComponent();
            OnPageAppearing();
        }

        private StackLayout returnMainLayout()
        {
            StackLayout finalLayout = new StackLayout();
            StackLayout firstLayout = new StackLayout { Orientation = StackOrientation.Horizontal, Margin = new Thickness(5,0,5,0) };

            Image arcimboldoLogo = new Image
            {
                Source = "arcimboldoLogo.jpg",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start
            };

            Label welcomeLabel = new Label
            {
                Text = "Bienvenue sur l'application Arcimboldo !",
                HorizontalTextAlignment = TextAlignment.Center,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.ForestGreen,
                Margin = new Thickness(0, 3, 0, 0),
                FontSize = 18
            };

            cityEntry = new CustomEntry
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "Choisissez une ville..",
                PlaceholderColor = Color.LightGray
            };

            cityEntry.TextChanged += OnCityEntryTextChange;

            Button validateButton = new Button
            {
                Text = "VALIDER",
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold,
                BackgroundColor = Color.FromHex("#80e27e"),
                HorizontalOptions = LayoutOptions.End
            };

            validateButton.Clicked += OnValidateClick;

            citiesListView = new ListView { IsVisible = false };
            citiesListView.ItemTapped += OnListViewItemTapped;

            firstLayout.Children.Add(cityEntry);
            firstLayout.Children.Add(validateButton);

            finalLayout.Children.Add(arcimboldoLogo);
            finalLayout.Children.Add(welcomeLabel);
            finalLayout.Children.Add(firstLayout);
            finalLayout.Children.Add(citiesListView);

            return finalLayout;
        }

        private async void OnCityEntryTextChange(object sender, TextChangedEventArgs e)
        {
            string newTextValue = e.NewTextValue;
            if (eventLock == 0 && newTextValue.Length >= 3)
            {
                List<string> citiesAndPostalCode = new List<string>();

                HttpClient client = new HttpClient();
                string content = await client.GetStringAsync("https://geo.api.gouv.fr/communes?nom=" + newTextValue + "&fields=nom,codesPostaux,centre&format=json&geometry=centre");
                ICollection<LocationClass> cities = JsonConvert.DeserializeObject<ICollection<LocationClass>>(content);
                IEnumerable<LocationClass> sortedCities = cities.OrderBy(x => x.CityName);

                citiesCoordinates = new List<Coordinates>();
                
                foreach (LocationClass location in sortedCities)
                {
                    if (location.PostalCodes.Length != 0 && location.centre != null)
                    {
                        citiesAndPostalCode.Add(location.CityName + " (" + location.PostalCodes[0] + ")");
                        JToken cityCoordonate = location.centre["coordinates"];
                        
                        citiesCoordinates.Add(new Coordinates
                        {
                            Latitude = (double)cityCoordonate[1],
                            Longitude = (double)cityCoordonate[0]
                        });
                    }
                }

                citiesListView.ItemsSource = citiesAndPostalCode;
                citiesListView.IsVisible = true;
            }
            else
            {
                citiesListView.IsVisible = false;
            }
        }

        private void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            eventLock = 1;

            string[] cityAndPostalCode = e.Item.ToString().Split('(');
            string cityName = cityAndPostalCode[0].Trim();
            string postalCode = cityAndPostalCode[1].Remove(cityAndPostalCode[1].Length - 1);

            CityAndPostalCode cityAndCode = new CityAndPostalCode
            {
                CityName = cityName,
                PostalCode = postalCode
            };

            chosenCityInformation = new UserCityInformation
            {
                CityNameAndPostalCode = cityAndCode,
                CityCoordinates = citiesCoordinates[e.ItemIndex]
            };

            citiesListView.IsVisible = false;
            cityEntry.Text = e.Item.ToString();

            eventLock = 0;
        }

        private async void OnValidateClick(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(cityEntry.Text) && chosenCityInformation != null)
            {
                NavigationPage navigationParent = this.Parent as NavigationPage;
                TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

                int pageCounter = tabbedParent.GetNumberOfTabs();

                await Task.Delay(500);
               
                if (pageCounter != 4 && String.IsNullOrEmpty(Settings.BasketProducts)) 
                {
                    Settings.UserLocation = JsonConvert.SerializeObject(chosenCityInformation);
                    tabbedParent.AddTabsAfterCityInput();
                    tabbedParent.SwitchToMainPage();
                }
                else if(pageCounter == 4 && String.IsNullOrEmpty(Settings.BasketProducts))
                {
                    Settings.UserLocation = JsonConvert.SerializeObject(chosenCityInformation);
                    tabbedParent.RefreshMainPageNavigationStack();

                    MessagingCenter.Send(this, "RefreshData");
                    tabbedParent.RefreshBasketNavigationStack();
                    tabbedParent.SwitchToMainPage();
                }
                else if(pageCounter == 4 && !String.IsNullOrEmpty(Settings.BasketProducts))
                {                   
                    UserCityInformation userCity = JsonConvert.DeserializeObject<UserCityInformation>(Settings.UserLocation);

                    if(userCity.CityNameAndPostalCode.CityName != chosenCityInformation.CityNameAndPostalCode.CityName)
                    {
                        bool changeLocation = await DisplayAlert("Changement de localisation", "Cette action va modifier les recettes disponibles, retirer du panier les produits non disponibles et changer les quantités en fonction de la nouvelle localisation, êtes-vous sûr ?", "Oui", "Non");

                        if (changeLocation == true)
                        {                           
                            Settings.UserLocation = JsonConvert.SerializeObject(chosenCityInformation);

                            tabbedParent.RefreshMainPageNavigationStack();

                            await RefreshContent();

                            MessagingCenter.Send(this, "RefreshData");
                            tabbedParent.RefreshBasketNavigationStack();
                            tabbedParent.SwitchToMainPage();
                        }
                    }
                    else
                    {
                        tabbedParent.RefreshMainPageNavigationStack();
                        tabbedParent.RefreshBasketNavigationStack();
                        tabbedParent.SwitchToMainPage();
                    }                    
                }
                else if(pageCounter != 4 && !String.IsNullOrEmpty(Settings.BasketProducts))
                {
                    UserCityInformation userCity = JsonConvert.DeserializeObject<UserCityInformation>(Settings.UserLocation);

                    if (userCity.CityNameAndPostalCode.CityName != chosenCityInformation.CityNameAndPostalCode.CityName)
                    {
                        bool changeLocation = await DisplayAlert("Changement de localisation", "Cette action va modifier les recettes disponibles, retirer du panier les produits non disponibles et changer les quantités en fonction de la nouvelle localisation, êtes-vous sûr ?", "Oui", "Non");

                        if (changeLocation == true)
                        {
                            tabbedParent.AddTabsAfterCityInput();

                            Settings.UserLocation = JsonConvert.SerializeObject(chosenCityInformation);

                            await RefreshContent();
                            tabbedParent.RefreshMainPageNavigationStack();

                            MessagingCenter.Send(this, "RefreshData");
                            tabbedParent.RefreshBasketNavigationStack();
                            tabbedParent.SwitchToMainPage();
                        }
                    }
                    else
                    {
                        tabbedParent.AddTabsAfterCityInput();
                        tabbedParent.SwitchToMainPage();
                    }
                }
            }
            else
            {
                await DisplayAlert("Problème de localisation", "Vous n'avez pas choisi une localisation de la liste.", "OK");
            }
        }

        private async Task RefreshContent()
        {
            HttpClient client = new HttpClient();
            Uri uri = new Uri("http://pfe32.ddns.net/availableProducts?latitude=" + chosenCityInformation.CityCoordinates.Latitude.ToString("G", CultureInfo.InvariantCulture) + "&longitude=" + chosenCityInformation.CityCoordinates.Longitude.ToString("G", CultureInfo.InvariantCulture));

            List<Models.JSONClasses.POST.ProductAsJson> productsAsJson = new List<Models.JSONClasses.POST.ProductAsJson>();
            List<Product> productsInBasket = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);

            foreach (Product p in productsInBasket)
            {
                productsAsJson.Add(new Models.JSONClasses.POST.ProductAsJson
                {
                    id = p.ProductID,
                    quantity = p.QuantityAsked
                });
            }

            string productsToJson = JsonConvert.SerializeObject(productsAsJson);
            StringContent content = new StringContent(productsToJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(uri, content);

            List<Models.JSONClasses.GET.ProductAsJson> updatedProductsAsJson = JsonConvert.DeserializeObject<List<Models.JSONClasses.GET.ProductAsJson>>(await response.Content.ReadAsStringAsync());

            List<Product> updatedProducts = new List<Product>();
            
            foreach (Product p in productsInBasket)
            {
                List<Models.JSONClasses.GET.ProductAsJson> productsAsJsonFiltered = updatedProductsAsJson.Where(x => x.Name == p.Name).ToList();

                if (productsAsJsonFiltered.Count > 0)
                {
                    Models.JSONClasses.GET.ProductAsJson productAsJson = productsAsJsonFiltered[0];

                    Product product = new Product
                    {
                        ProductID = productAsJson.ProductID,
                        Name = productAsJson.Name,
                        Description = productAsJson.Description,
                        QuantityInStock = productAsJson.QuantityInStock,
                        UnitType = productAsJson.UnitType,
                        Price = productAsJson.Price,
                        ImageLink = productAsJson.ImageLink
                    };

                    Producer producer = new Producer
                    {
                        IdProducer = Int32.Parse(productAsJson.Producer["idProducer"].ToString()),
                        Name = productAsJson.Producer["nameProducer"].ToString(),
                        Mail = productAsJson.Producer["emailContact"].ToString(),
                        PhoneNumber = productAsJson.Producer["phoneNumber"].ToString()
                    };

                    List<MarketDay> marketDays = new List<MarketDay>();

                    foreach (JToken marketDay in productAsJson.Producer["marketDays"])
                    {
                        JToken JSONMarket = marketDay["market"];

                        MarketDay day = new MarketDay { Day = marketDay["dayMarketDay"].ToString() };

                        JToken JSONAddress = JSONMarket["address"];

                        Market market = new Market
                        {
                            IdMarket = Int32.Parse(JSONMarket["idMarket"].ToString()),
                            Address = new Address
                            {
                                IdAddress = Int32.Parse(JSONAddress["idAddress"].ToString()),
                                Street = JSONAddress["streetAddress"].ToString(),
                                City = JSONAddress["cityAddress"].ToString(),
                                Country = JSONAddress["countryAddress"].ToString(),
                                PostalCode = Int32.Parse(JSONAddress["zipCodeAddress"].ToString()),
                                Latitude = Double.Parse(JSONAddress["latitudeAddress"].ToString()),
                                Longitude = Double.Parse(JSONAddress["longitudeAddress"].ToString())
                            }
                        };

                        day.Market = market;
                        marketDays.Add(day);
                    }

                    producer.MarketDays = marketDays;
                    product.Producer = producer;

                    if (p.QuantityAsked > productAsJson.QuantityInStock)
                    {
                        product.QuantityAsked = productAsJson.QuantityInStock;
                    }
                    else
                    {
                        product.QuantityAsked = p.QuantityAsked;
                    }

                    updatedProducts.Add(product);
                }
            }

            Settings.BasketProducts = JsonConvert.SerializeObject(updatedProducts);
        }

        private async void OnPageAppearing()
        {
            try
            {
                GeolocationRequest request = new GeolocationRequest(GeolocationAccuracy.Best);
                Location location = await Geolocation.GetLocationAsync(request);

                if (location != null)
                {
                    this.Content = returnMainLayout();
                    
                    HttpClient client = new HttpClient();
                    string content = await client.GetStringAsync("https://geo.api.gouv.fr/communes?lat=" + location.Latitude.ToString("G", CultureInfo.InvariantCulture) + "&lon=" + location.Longitude.ToString("G", CultureInfo.InvariantCulture) + "&fields=nom,codesPostaux&format=json&geometry=contour");
                    List<LocationClass> cities = JsonConvert.DeserializeObject<ICollection<LocationClass>>(content).ToList();

                    eventLock = 1;
                    cityEntry.Text = cities[0].CityName + " (" + cities[0].PostalCodes[0] + ")";
                    eventLock = 0;

                    chosenCityInformation = new UserCityInformation
                    {
                        CityNameAndPostalCode = new CityAndPostalCode
                        {
                            CityName = cities[0].CityName,
                            PostalCode = cities[0].PostalCodes[0]
                        },
                        CityCoordinates = new Coordinates
                        {
                            Latitude = location.Latitude,
                            Longitude = location.Longitude
                        }
                    };
                }
            }
            catch (PermissionException pEx)
            {
                this.Content = returnMainLayout();
            }
            catch (Exception ex)
            {
                await DisplayAlert("Problème de localisation", "Nous n'avons pas pu vous localiser.", "OK");
                this.Content = returnMainLayout();
            }
        }
    }
}