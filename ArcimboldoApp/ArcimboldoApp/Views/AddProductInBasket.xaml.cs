﻿using ArcimboldoApp.Models;
using ArcimboldoApp.Models.CodeClasses;
using ArcimboldoApp.Models.DatabaseClasses;
using ArcimboldoApp.Models.JSONClasses.GET;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArcimboldoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddProductInBasket : ContentPage
    {
        ScrollView productListScroll;
        List<Product> products = new List<Product>();
        public AddProductInBasket()
        {
            InitializeComponent();

            StackLayout finalLayout = new StackLayout();

            CustomEntry productEntry = new CustomEntry 
            { 
                Placeholder = "Entrez le nom d'un produit...", 
                PlaceholderColor = Color.LightGray,
                Margin = new Thickness(20,10,20,5)
            };

            productEntry.TextChanged += OnProductEntryTextChanged;

            productListScroll = new ScrollView { Orientation = ScrollOrientation.Vertical };

            finalLayout.Children.Add(productEntry);
            finalLayout.Children.Add(productListScroll);

            this.Content = finalLayout;
            
        }

        private async void OnProductEntryTextChanged(object sender, TextChangedEventArgs e)
        {
            string newTextValue = e.NewTextValue;

            if(newTextValue.Length >= 3)
            {
                UserCityInformation userCity = JsonConvert.DeserializeObject<UserCityInformation>(Settings.UserLocation);

                HttpClient client = new HttpClient();
                string content = await client.GetStringAsync("http://pfe32.ddns.net/productSearch/" + newTextValue + "?latitude=" + userCity.CityCoordinates.Latitude.ToString("G", CultureInfo.InvariantCulture) + "&longitude=" + userCity.CityCoordinates.Longitude.ToString("G", CultureInfo.InvariantCulture));

                ICollection<ProductAsJson> productsAsJson = JsonConvert.DeserializeObject<ICollection<ProductAsJson>>(content);
                
                StackLayout productListLayout = new StackLayout();

                if (productsAsJson.Any())
                {
                    foreach (ProductAsJson p in productsAsJson)
                    {
                        Product product = new Product
                        {
                            ProductID = p.ProductID,
                            Name = p.Name,
                            Description = p.Description,
                            QuantityInStock = p.QuantityInStock,
                            UnitType = p.UnitType,
                            Price = p.Price,
                            ImageLink = p.ImageLink
                        };

                        Producer producer = new Producer
                        {
                            IdProducer = Int32.Parse(p.Producer["idProducer"].ToString()),
                            Name = p.Producer["nameProducer"].ToString(),
                            Mail = p.Producer["emailContact"].ToString(),
                            PhoneNumber = p.Producer["phoneNumber"].ToString()
                        };

                        List<MarketDay> marketDays = new List<MarketDay>();

                        foreach (JToken marketDay in p.Producer["marketDays"])
                        {
                            JToken JSONMarket = marketDay["market"];

                            MarketDay day = new MarketDay { Day = marketDay["dayMarketDay"].ToString() };

                            JToken JSONAddress = JSONMarket["address"];

                            Market market = new Market
                            {
                                IdMarket = Int32.Parse(JSONMarket["idMarket"].ToString()),
                                Address = new Address
                                {
                                    IdAddress = Int32.Parse(JSONAddress["idAddress"].ToString()),
                                    Street = JSONAddress["streetAddress"].ToString(),
                                    City = JSONAddress["cityAddress"].ToString(),
                                    Country = JSONAddress["countryAddress"].ToString(),
                                    PostalCode = Int32.Parse(JSONAddress["zipCodeAddress"].ToString()),
                                    Latitude = Double.Parse(JSONAddress["latitudeAddress"].ToString()),
                                    Longitude = Double.Parse(JSONAddress["longitudeAddress"].ToString())
                                }
                            };

                            day.Market = market;
                            marketDays.Add(day);
                        }

                        producer.MarketDays = marketDays;
                        product.Producer = producer;

                        products.Add(product);

                        Frame productFrame = new Frame 
                        { 
                            AutomationId = "Frame:" + p.ProductID,
                            BorderColor = Color.Black,
                            CornerRadius = 30,
                            Margin = new Thickness(5,0,5,0)
                        };

                        StackLayout finalProductLayout = new StackLayout();

                        StackLayout firstProductLayout = new StackLayout 
                        { 
                            AutomationId = "ProductCaracteristics",
                            Orientation = StackOrientation.Horizontal 
                        };

                        StackLayout secondProductLayout = new StackLayout
                        {
                            AutomationId = "ProductDescription",
                            IsVisible = false
                        };

                        StackLayout thirdProductLayout = new StackLayout { Orientation = StackOrientation.Horizontal };

                        StackLayout producerImageLayout = new StackLayout
                        {
                            HorizontalOptions = LayoutOptions.Start,
                            Margin = new Thickness(0,3,0,0)
                        };

                        Image productImage = new Image
                        {
                            Source = p.ImageLink,
                            HeightRequest = 55,
                            WidthRequest = 55
                        };

                        FormattedString productCaracteristicsText = new FormattedString();
                        productCaracteristicsText.Spans.Add(new Span { Text = p.Name, ForegroundColor = Color.FromHex("#087f23"), FontAttributes = FontAttributes.Bold });
                        productCaracteristicsText.Spans.Add(new Span { Text = "\n" });
                        productCaracteristicsText.Spans.Add(new Span { Text = "Prix : " });
                        productCaracteristicsText.Spans.Add(new Span { Text = p.Price + "€/" + p.UnitType.ToLower() });

                        Label productCaracteristics = new Label
                        {
                            FormattedText = productCaracteristicsText,
                            VerticalTextAlignment = TextAlignment.Center
                        };

                        Image producerImage = new Image
                        {
                            HeightRequest = 40,
                            WidthRequest = 40,
                            Source = "login.png"
                        };

                        ImageButton addToBasket = new ImageButton
                        {
                            AutomationId = "AddProduct:" + p.ProductID,
                            Source = "addToCart.png",
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.End,
                            HeightRequest = 25,
                            WidthRequest = 25,
                            BackgroundColor = Color.Transparent
                        };

                        addToBasket.Clicked += OnAddProductClicked;

                        StackLayout questionMarkLayout = new StackLayout 
                        { 
                            AutomationId = "QuestionMark:" + p.ProductID,
                            HeightRequest = 25,
                            WidthRequest = 25,
                            VerticalOptions = LayoutOptions.Center,
                            BackgroundColor = Color.Transparent,
                            HorizontalOptions = LayoutOptions.EndAndExpand,
                        };
                        TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
                        tapGestureRecognizer.Tapped += OnQuestionMarkClicked;

                        questionMarkLayout.GestureRecognizers.Add(tapGestureRecognizer);

                        Image questionMark = new Image { Source = "questionmark.png" };

                        string[] productDescription = p.Description.Split(' ');
                        FormattedString productDescriptionText = new FormattedString();

                        foreach (string word in productDescription)
                        {
                            if(word == "Robe" || word == "Production" || word == "Saveur" || word == "Préparations")
                            {
                                productDescriptionText.Spans.Add(new Span { Text = "\n" + word  + "\n", ForegroundColor = Color.DarkGray, FontAttributes = FontAttributes.Bold, FontSize = 16 });
                                productDescriptionText.Spans.Add(new Span { Text = " " });
                            }
                            else
                            {
                                productDescriptionText.Spans.Add(new Span { Text = word + " " });
                            }
                        }

                        Label descriptionText = new Label
                        {
                            HorizontalTextAlignment = TextAlignment.Center,
                            FormattedText = productDescriptionText
                        };

                        FormattedString producerDescriptionText = new FormattedString();
                        producerDescriptionText.Spans.Add(new Span { Text = "Producteur : ", ForegroundColor = Color.FromHex("#087f23"), FontAttributes = FontAttributes.Bold, FontSize = 16 });
                        producerDescriptionText.Spans.Add(new Span { Text = "\n" });
                        producerDescriptionText.Spans.Add(new Span { Text = producer.Mail });
                        producerDescriptionText.Spans.Add(new Span { Text = "\n" });
                        producerDescriptionText.Spans.Add(new Span { Text = producer.PhoneNumber });

                        Label producerDescription = new Label
                        {
                            FormattedText = producerDescriptionText,
                            VerticalOptions = LayoutOptions.StartAndExpand,
                            HorizontalOptions = LayoutOptions.CenterAndExpand
                        };

                        Image goToProducerPage = new Image
                        {
                            Source = "rightarrow.png",
                            HorizontalOptions = LayoutOptions.EndAndExpand,
                            HeightRequest = 25,
                            WidthRequest = 25
                        };

                        producerImageLayout.Children.Add(producerImage);
                        producerImageLayout.Children.Add(new Label { Text = producer.Name, HorizontalTextAlignment = TextAlignment.Center });

                        questionMarkLayout.Children.Add(questionMark);

                        firstProductLayout.Children.Add(productImage);
                        firstProductLayout.Children.Add(productCaracteristics);
                        firstProductLayout.Children.Add(questionMarkLayout);
                        firstProductLayout.Children.Add(addToBasket);

                        thirdProductLayout.Children.Add(producerImageLayout);
                        thirdProductLayout.Children.Add(producerDescription);
                        thirdProductLayout.Children.Add(goToProducerPage);

                        secondProductLayout.Children.Add(new BoxView
                        {
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            HeightRequest = 0.5,
                            Color = Color.FromHex("#000000")
                        });
                        secondProductLayout.Children.Add(descriptionText);

                        secondProductLayout.Children.Add(new BoxView
                        {
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            HeightRequest = 0.5,
                            Color = Color.FromHex("#000000")
                        });
                        secondProductLayout.Children.Add(thirdProductLayout);

                        finalProductLayout.Children.Add(firstProductLayout);
                        finalProductLayout.Children.Add(secondProductLayout);

                        productFrame.Content = finalProductLayout;

                        productListLayout.Children.Add(productFrame);                    
                    }
                   
                    productListScroll.Content = productListLayout;
                }
                else
                {
                    StackLayout emptyResearch = new StackLayout();
                    Image sadOnion = new Image
                    {
                        Source = "sadonion.png",
                        HorizontalOptions = LayoutOptions.Center,
                        BackgroundColor = Color.Transparent,
                        WidthRequest = 65,
                        HeightRequest = 65,
                        Margin = new Thickness(0, 0, 0, 5)
                    };

                    Label informationLabel = new Label
                    {
                        Text = "Malheureusement,\nAucun produit correspondant à votre recherche n'a été trouvé.",
                        FontAttributes = FontAttributes.Bold,
                        HorizontalTextAlignment = TextAlignment.Center
                    };

                    emptyResearch.Children.Add(sadOnion);
                    emptyResearch.Children.Add(informationLabel);

                    productListScroll.Content = emptyResearch;
                }
            }
        }
        private void OnQuestionMarkClicked(object sender, EventArgs e)
        {
            StackLayout correspondingQuestionMark = (StackLayout)sender;
            correspondingQuestionMark.Children.RemoveAt(0);

            Image newQuestionMark = new Image();

            string[] splitId = correspondingQuestionMark.AutomationId.Split(':');

            Product correspondingProduct = products.Find(x => x.ProductID == Int32.Parse(splitId[1]));

            foreach(StackLayout i in productListScroll.Children.Where(x => x.GetType() == typeof(StackLayout)))
            {
                foreach (Frame j in i.Children.Where(x => x.GetType() == typeof(Frame)))
                {
                    if (splitId[1] == j.AutomationId.Split(':')[1])
                    {
                        foreach (StackLayout k in j.Children.Where(x => x.GetType() == typeof(StackLayout)))
                        {
                            foreach (StackLayout l in k.Children.Where(x => x.GetType() == typeof(StackLayout)))
                            {
                                if (l.AutomationId == "ProductDescription")
                                {
                                    if (l.IsVisible == true)
                                    {                                     
                                        newQuestionMark.Source = "questionmark.png";
                                        l.IsVisible = false;
                                    }
                                    else
                                    {
                                        newQuestionMark.Source = "enabledquestionmark.png";
                                        l.IsVisible = true;
                                    }
                                    correspondingQuestionMark.Children.Add(newQuestionMark);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void OnAddProductClicked(object sender, EventArgs e)
        {
            ImageButton correspondingArrow = (ImageButton)sender;
            string[] splitId = correspondingArrow.AutomationId.Split(':');

            Product correspondingProduct = products.Find(x => x.ProductID == Int32.Parse(splitId[1]));
            List<Product> productsInBasket = JsonConvert.DeserializeObject<List<Product>>(Settings.BasketProducts);
            
            if (productsInBasket == null) { productsInBasket = new List<Product>(); }

            Product foundProduct = productsInBasket.Find(x => x.ProductID == correspondingProduct.ProductID);

            if (foundProduct == null) 
            {
                correspondingProduct.QuantityAsked = 1;
                productsInBasket.Insert(0, correspondingProduct); 
            }
            else 
            { 
                foundProduct.QuantityAsked += 1;
            }

            Settings.BasketProducts = JsonConvert.SerializeObject(productsInBasket);

            NavigationPage navigationParent = this.Parent as NavigationPage;
            TabbedBar tabbedParent = navigationParent.Parent as TabbedBar;

            MessagingCenter.Send(this, "RefreshData");
            Navigation.RemovePage(this);
        }
    }
}